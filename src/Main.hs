module Main where

--
import Carcaskell.GUI
import Carcaskell.Logic
import Codec.BMP
import qualified Data.ByteString as B
-- Graphics imports
import Codec.BMP
import Graphics.Gloss.Data.Color
import Graphics.Gloss.Data.Picture
--
import Control.Monad
import System.Directory (setCurrentDirectory)
import System.Random (getStdGen)
import System.Random.Shuffle (shuffle')

-- |Colorizes the bitmap.
recolor :: (Float, Float, Float) -> BMP -> BMP
recolor (rc, gc, bc) bmp@BMP {bmpRawImageData = raw} = bmp {bmpRawImageData = B.pack $ process $ B.unpack raw} where
    process (b:g:r:a:xs) = (mul b bc):(mul g gc):(mul r rc):a:process xs
    process xs = xs
    mul c cc = round $ cc * fromIntegral c

-- |Loads the tileset form a config file.
loadTileset :: FilePath -> IO Box
loadTileset filename = do
    records <- liftM read $ readFile filename
    liftM concat $ forM records $ \(spriteFile, regions, count) -> do
        sprite <- loadBMP spriteFile
        return $ replicate count (Tile regions sprite 0)

main = do
    setCurrentDirectory "../img"
    start:tiles <- loadTileset "testSet.dat"
    gen <- getStdGen
    let first:box = shuffle' tiles (length tiles) gen
    -- Hardcode for testing
    Right bmp <- readBMP "../img/follower.bmp" -- ...and for this
    let fSpriteR = bitmapOfBMP $ recolor (1, 0.2, 0.2) $ bmp
        fSpriteG = bitmapOfBMP $ recolor (0.2, 1, 0.2) $ bmp
        fSpriteBl = bitmapOfBMP $ recolor (0.2, 0.2, 0.2) $ bmp
        playerR = Player red "Redd" fSpriteR
        playerG = Player green "Greenie" fSpriteG
        playerBl = Player black "Blacky" fSpriteBl
        players = [playerR, playerG, playerBl]
    playCarcaskell $ initState
        { phase = PlaceTile first
        , box = box
        , field = placeTile (0, 0) start emptyField
        , turns = cycle players
        , players = initPlayers players
        }
