module Carcaskell.Logic where

import Data.Function (on)
import qualified Data.Map.Strict as M
import Data.Map ((!))
import qualified Data.Set as S
import Data.Maybe
import Data.List

-- Graphics part. TODO: separate?
import Graphics.Gloss.Data.Color
import Graphics.Gloss.Data.Picture hiding (rotate)
import Graphics.Gloss.Data.Point

-- |A player
data Player = Player
    { plColor          :: !Color   -- ^Color of a player and their followers
    , plName           :: !String  -- ^Player name
    , plFollowerSprite :: !Picture -- ^Sprite for player followers
    }

instance Eq Player where
    (==) = (==) `on` plName

instance Ord Player where
    compare = compare `on` plName

-- |An enum describing what is currently going on the field.
data Phase = PlaceTile Tile | PlaceFollower Cell | EndGame

-- |An edge of the square tile. Edges are enumerated clockwise, from the top to the left.
-- |Halfs are for farms.
-- |                  ETop
-- |                 ___^___
-- |                /       \
-- |               HTop1 HTop2
-- |       /        *---*---*         \
-- |       | HLeft2 |.'.H./#| HRight1 |
-- | ELeft<         *===o.|#*          >ERight
-- |       | HLeft1 |.'.H.\#| HRight2 |
-- |       \        *---*---*         /
-- |            HBottom2 HBottom1
-- |                \___ ___/
-- |                    v
-- |                 EBottom
data Edge = ETop     | ERight   | EBottom | ELeft
          | HTop1    | HTop2    | HRight1 | HRight2
          | HBottom1 | HBottom2 | HLeft1  | HLeft2
    deriving (Eq, Enum, Ord, Show, Read)

-- |Returns the opposite edge.
oppositeEdge :: Edge -> Edge
oppositeEdge x | x < HTop1 = toEnum $ (fromEnum x + 2) `rem` 4
               | otherwise = let x' = fromEnum x - 4 in
                             --
                             toEnum $ 4 + (x' + 5 - 2 * (x' `rem` 2)) `rem` 8

data CityMark = N | Pennant
    deriving (Eq, Ord, Show, Read)

-- |A region of the tile where a follover can be placed.
data Region = Cloister 
            | City CityMark [Edge] -- ^City connects some edges.
            | Farm [Edge]          -- ^Farm connects some of edges or their halfs.
            | Road [Edge]          -- ^Road connects some edges.
    deriving (Eq, Ord, Show, Read)

-- |Returns all edges of a region.
regionEdges Cloister = []
regionEdges (City _ es) = es
regionEdges (Farm es) = es
regionEdges (Road es) = es

data RegionType = TCloister | TCity | TFarm | TRoad
    deriving Eq

-- |Returns the type of a region.
regionType Cloister   = TCloister
regionType (City _ _) = TCity
regionType (Farm _)   = TFarm
regionType (Road _)   = TRoad

-- |A tile data type which encapsulates the graphic tile and its (integer) rotation.
data Tile = Tile
    { regions :: [(Point, Region)]
    , sprite  :: !Picture
    , angle   :: !Int
    }

instance Show Tile where
   show Tile {regions = regions} = "Tile {regions = " ++ show regions ++ "}"

-- |A box of tiles.
type Box = [Tile]

-- |A data type for followers.
data Follower = Follower
    { owner :: !Player -- ^Which player placed him
    }

-- |A cell of the field.
type Cell = (Int, Int)

-- |An exact pinpoint of tile location for follower placement.
type Place = (Cell, Region)

-- |A field of tiles.
data Field = Field
    { tiles     :: !(M.Map Cell Tile)
    , followers :: !(M.Map Place (Point, Follower))
    }

-- |A field before the game.
emptyField :: Field
emptyField = Field M.empty M.empty

-- |A class of types which can be rotated by 90 degrees clockwise.
class Rotate a where
    rotate :: a -> a

instance Rotate Edge where
    rotate ELeft  = ETop
    rotate HLeft1 = HTop1
    rotate HLeft2 = HTop2
    rotate x | x < ELeft = succ x
             | otherwise = succ (succ x)

instance Rotate Region where
    rotate (Road borders)   = Road $ map rotate borders
    rotate (City m borders) = City m $ map rotate borders
    rotate (Farm borders)   = Farm $ map rotate borders
    rotate x = x

-- For avoiding of FlexibleInstances
rotateV (x, y) = (y, -x)

-- To rotate tile, rotate all of its regions and the sprite
instance Rotate Tile where
    rotate tile@Tile {regions = regions, angle = angle} =
        tile {regions = map (\(p, r) -> (rotateV p, rotate r)) regions, angle = angle + 1}

type Score = Int

-- |A type for holding the player state.
data PlayerState = PS
    { availFollowers :: Int
    , score          :: Score
    }

type PlayerTable = M.Map Player PlayerState

-- |A type for holding the game state.
data LogicState = LS
    { phase      :: !Phase        -- ^Phase of current turn
    , field      :: !Field        -- ^Field (tiles and followers)
    , box        :: !Box          -- ^Rest of the tiles
    , turns      :: [Player]      -- ^Infinite list of player turns
    , players    :: !PlayerTable  -- ^Player states
    }

-- |Initial state.
initState = LS
    { phase = EndGame
    , field = emptyField
    , box = []
    , turns = []
    , players = M.empty
    }

-- Functional interface part ---------------------------------------------------------------

-- |Returns all tiles connected to a tile.
neighbourTiles :: Cell -> Field -> [(Cell, Tile)]
neighbourTiles (x, y) field = catMaybes neighbourList where
    neighbourList = map findCell [(x-1, y), (x, y-1), (x, y+1), (x+1, y)] 
    findCell x = do  
        tile <- M.lookup x $ tiles field
        return (x, tile)

-- |Returns the contact edges of two cells (if they are adjacent).
neighbourEdge :: Cell -> Cell -> Maybe (Edge, Edge)
neighbourEdge (x1, y1) (x2, y2)
    | x1 == x2 + 1 && y1 == y2 = Just (ELeft, ERight)
    | x1 == x2 - 1 && y1 == y2 = Just (ERight, ELeft)
    | y1 == y2 + 1 && x1 == x2 = Just (EBottom, ETop)
    | y1 == y2 - 1 && x1 == x2 = Just (ETop, EBottom)
    | otherwise = Nothing

-- |Returns the region of tile which contains the particular edge.
-- |Throws an error if there aren't any.
getRegion :: Tile -> Edge -> Region
getRegion tile edge = case find hasEdge $ map snd $ regions tile of 
    Nothing -> error $ show tile ++ " has no edge " ++ show edge
    Just r  -> r
    where
    hasEdge Cloister = False
    hasEdge region = edge `elem` regionEdges region

-- |Check if a tile can be placed on the field cell.
tileCanBePlaced :: Cell -> Field -> Tile -> Bool
tileCanBePlaced c f t = case neighbourTiles c f of
    [] -> False
    neighbours -> all valid neighbours where
        getRegionType tile = regionType . getRegion tile
        valid (cell, tile) = getRegionType t edge1 == getRegionType tile edge2 where
            (edge1, edge2) = fromJust $ neighbourEdge c cell 

-- |Place a tile on the field.
placeTile :: Cell -> Tile -> Field -> Field
placeTile cell tile field =
    field {tiles = M.insert cell tile (tiles field)}

-- |Check if a follower can be placed on the tile region.
followerCanBePlaced :: Field -> Cell -> Region -> Bool
followerCanBePlaced f c r = all free $ objectRegions $ getObject f c r where
    free place = isNothing $ M.lookup place $ followers f where

-- |Returns an offset for a cell which should contact with some edge of a tile.
edgeOffset :: Edge -> (Int, Int)
edgeOffset e = [(0,1), (1,0), (0,-1), (-1,0), (0,1), (0,1), (1,0), (1,0), (0,-1), (0,-1), (-1,0), (-1,0)] !! fromEnum e

type Object = [Place]
type ObjectInfo = (Object, Bool)
objectRegions = fst
isCompleted = snd

-- |Returns all neighbour regions which directly contact to some region of tile.
adjacentRegions :: Field -> Cell -> Region -> ObjectInfo
adjacentRegions f c Cloister = ([], False)
adjacentRegions f c r = (catMaybes res, all isJust res) where
    res = map findAdjacent $ regionEdges r
    findAdjacent edge = do
        let cell = c `addV` edgeOffset edge
        tile <- M.lookup cell (tiles f)
        return $ (cell, getRegion tile (oppositeEdge edge))
    addV (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

-- |Returns all regions which form some object on a map (starting with some region).
getObject :: Field -> Cell -> Region -> ObjectInfo
getObject f = runRegions S.empty where
    runRegions visited c r = ((c, r) : concat next, and closed && cur) where
        (next, closed) = unzip $ map (uncurry $ runRegions (S.insert c visited)) adjacent
        (temp, cur) = adjacentRegions f c r
        adjacent = filter (\(c', _) -> S.notMember c' visited) temp

-- |Place a follower on the tile region. 
placeFollower :: Place -> Point -> Follower -> Field -> Field
placeFollower place point follower field =
    field {followers = M.insert place (point, follower) (followers field)}

-- |Starting (empty) score table.
initPlayers :: [Player] -> PlayerTable
initPlayers players = M.fromList $ map makePS players where
    makePS player = (player, PS {availFollowers = 6, score = 0})

-- |Add score for a player.
addScore :: Player -> Score -> PlayerTable -> PlayerTable
addScore player score table = M.adjust addScore player table where
    addScore state@PS {score = oldScore} = state {score = oldScore + score}

winnerWithTies :: (a -> a -> Ordering) -> [a] -> [a]
winnerWithTies f xs = case sortBy (compare `on` snd) $ map (\g@(x:_) -> (x, length g)) $ groupBy (\a b -> f a b == EQ) $ sortBy f xs of
    [] -> []
    sorted@((_, winnerScore):_) -> map fst $ takeWhile ((== winnerScore) . snd) sorted where

regionScore :: Region -> Score
regionScore Cloister = 1
regionScore (Road _) = 1
regionScore (City N _) = 2
regionScore (City Pennant _) = 4
regionScore _ = 0

calcScore :: Object -> Score
calcScore = sum . map (regionScore . snd)

-- |Checks if a freshly tile completes some objects.
-- |Removes their followers from the field and returns a score table along with them.
completeObjects :: Field -> Cell -> (Field, [Follower], [(Player, Score)])
completeObjects field cell = runComplete (map (getObject field cell . snd) $ regions (tiles field ! cell)) (followers field) [] [] where
    runComplete [] folMap folList scores = (field {followers = folMap}, folList, scores)
    runComplete ((_, False):os) folMap folList scores = runComplete os folMap folList scores
    runComplete ((o, True):os) folMap folList scores = runComplete os newMap (fList ++ folList) (winners ++ scores) where
        (newMap, fList) = runFol o folMap []
        runFol [] m l = (m, l)
        runFol (p:ps) m l = case M.lookup p $ followers field of
            Nothing     -> runFol ps m l
            Just (_, f) -> runFol ps (M.delete p m) (f:l)
        winners = [(owner f, calcScore o) | f <- winnerWithTies (compare `on` owner) $ fList]
    -- TODO: find completed cloisters
