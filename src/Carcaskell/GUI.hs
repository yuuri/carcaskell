module Carcaskell.GUI where

import Carcaskell.Logic hiding (rotate)
import qualified Carcaskell.Logic as Logic (rotate)
import Data.List (sortBy)
import qualified Data.Map as M
import Data.Ord (comparing)
import Graphics.Gloss.Data.ViewState
import Graphics.Gloss.Data.Vector
import Graphics.Gloss.Interface.Pure.Game
import Text.Printf

-- Rendering functions --------------------------------------------------------------------

-- |Tile size (must be square)
tileSize = 86
threshold = 16

(windowWidth, windowHeight) = (1024, 600)

-- |Snaps the coordinates to the tile grid.
snapGrid :: Point -> Cell
snapGrid (x, y) = (snap x, snap y) where
    snap c = floor $ c / tileSize + 0.5

-- |Restores the screen coordinates from the tile.
unsnapGrid :: Cell -> Point
unsnapGrid (x, y) = (unsnap x, unsnap y) where
    unsnap c = tileSize * fromIntegral c

-- |Distance between two points (vectors).
distV (x1, y1) (x2, y2) = sqrt ((x1 - x2) ^ 2 + (y1 - y2) ^ 2)

-- |Vector sum.
addV (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

-- |Gets an absolute position of an offset point on the cell.
offsetPosition :: Cell -> Point -> Point
offsetPosition cell offset = unsnapGrid cell `addV` (tileSize `mulSV` offset)

-- |Sorts the tile regions by distance to mouse in the ascending order (first is the nearest).
sortTileRegions :: Cell -> Tile -> Point -> [(Point, Region)]
sortTileRegions cell tile mouse = sortByDist $ map move $ regions tile where
    sortByDist = sortBy (comparing (distV mouse . fst))
    move (offset, region) = (offsetPosition cell offset, region)

data InterfaceState = IS
    { mouse :: !Point
    , viewState :: !ViewState
    }

data GameState = GS
    { ls :: !LogicState
    , is :: !InterfaceState
    }

-- |Translates the screen mouse coordinates to the field coordinates.
stateMouse :: InterfaceState -> Point -> Point
stateMouse state = invertViewPort (viewStateViewPort $ viewState state)

-- |Render the `world'
render state@GS
    { ls = LS
        { phase = phase
        , field = field@Field {tiles = tiles, followers = followers}
        , turns = currentPlayer:_
        , players = players
        }
    , is = IS
        { mouse = mouse
        , viewState = ViewState {viewStateViewPort = port}
        }
    } = pictures [applyViewPortToPicture port (pictures
         [ drawTiles
         , drawFollowers
         , renderPhase phase
         ]), drawPlayersTable] where

    -- To draw a tile, move the sprite and draw it
    drawTile (x, y) Tile {sprite = sprite, angle = angle} =
        translate x y (rotate (fromIntegral $ 90 * angle) sprite)

    -- Same with followers
    drawFollower (x, y) = translate x y $ plFollowerSprite currentPlayer

    -- To draw all tiles, restore their positions from cell indexes
    drawTiles = pictures $
        M.foldrWithKey (\cell tile acc -> drawTile (unsnapGrid cell) tile : acc) [] tiles
        --M.toList $ M.map drawTile $ tiles

    drawFollowers = pictures $ map move (M.assocs followers) where
        move ((cell, _), ((tx, ty), follower)) = translate tx ty $ plFollowerSprite $ owner follower

    -- Draw the snapping rectangle, and the tile being placed
    renderPhase (PlaceTile tile) = pictures [drawRect, drawTile mouse tile] where
        drawRect = color red $ translate tx ty (rectangleWire tileSize tileSize)
        (tx, ty) = unsnapGrid $ snapGrid mouse

    -- Draw the snapping rectangle(s), and the follower being placed
    renderPhase (PlaceFollower cell) = pictures [drawFollower mouse, move rc] where
        (rc, region) = head $ sortTileRegions cell (tiles M.! cell) mouse
        move tc@(tx, ty) = translate tx ty $ color rectColor $ rectangleWire 10 10 where
            --When a follower is placeable, hint rectangle is green
            checkPlace = tc `distV` mouse < threshold && followerCanBePlaced field cell region
            rectColor = if checkPlace then green else red

    -- Draw nothing special
    renderPhase EndGame = blank

    -- Always draw the player states
    drawPlayersTable = translate (-windowWidth / 2 + 120) (windowHeight / 2 - 120) $
       pictures $ map drawState enumPlayers where

       enumPlayers = zip [0 ..] $ M.assocs players
       drawState (i, ( player@Player {plName = pname, plColor = pcolor}
                     , state@PS {availFollowers = n, score = score})) =
           --Move and scale
           translate 0 (fromIntegral i * 30) $ pictures
               [ translate (-16) 12 $ color black $
                   if player == currentPlayer then circle 12 else blank
               , scale fontSize fontSize $
                   color pcolor $ text $ printf "%s: %d, %d" pname n score
               ]
       fontSize = 0.2

-- |Function which updates the world. As long as all updates occur during
-- |the events, it does nothing.
step _ = id

-- |Ends the current turn, calculates the score for completed objects,
-- |and moves on to the next turn if the tile box is non-empty.
nextTurn :: GameState -> GameState
nextTurn state@GS {ls = LS {phase = EndGame}} = state
nextTurn state@GS
    { ls = ls@LS
        { phase = PlaceFollower cell
        , turns = player:turns
        , box = box
        , players = players
        , field = field
        }
    } = state
    { ls = ls 
        { phase = newPhase
        , turns = turns
        , box = newBox
        , players = newPlayers
        , field = newField
        }
    } where
    scoredPlayers = foldl addCurScore players objects
    addCurScore table (player, score) = addScore player score table
    newPlayers = foldl returnFollower scoredPlayers remFollowers
    returnFollower table follower = M.adjust ret (owner follower) table
    ret state@PS {availFollowers = n} = state {availFollowers = n + 1}
    --objects :: [(Player, Int)]
    (newField, remFollowers, objects) = completeObjects field cell
    (newPhase, newBox) = case box of
        []        -> (EndGame, [])
        tile:rest -> (PlaceTile tile, rest)
    

-- Game events ------------------------------------------------------------

-- |Process the viewport events with a handler, and the rest with clauses below.
events event state@GS {is = is@IS {viewState = viewState}} =
    gameEvents event state {is = is {viewState = updateViewStateWithEvent event viewState}}

-- |Can't do anything at the end 
gameEvents _ state@GS {ls = LS {phase = EndGame}} = state

-- |Rotate the current tile
gameEvents (EventKey (MouseButton RightButton) Down _ _)
    state@GS {ls = ls@LS {phase = PlaceTile tile}} =
        state {ls = ls {phase = PlaceTile $ Logic.rotate tile}}

-- |Place the tile
gameEvents (EventKey (MouseButton LeftButton) Down _ mouse)
    state@GS
        { ls = ls@LS
            { phase   = PlaceTile tile
            , field   = field
            , turns   = player:_
            , players = players
            }
        } = case (checkPlace, hasFollowers) of
    (False, _)    -> state -- Tile wasn't placed
    (True, True)  -> newState -- Tile was placed and the current player has no followers to place
    (True, False) -> nextTurn newState
    where
        hasFollowers = availFollowers (players M.! player) > 0
        tc = snapGrid $ stateMouse (is state) mouse
        checkPlace = tileCanBePlaced tc field tile
        newField = if checkPlace then placeTile tc tile field else field
        newState = state
            { ls = ls
                { phase = PlaceFollower tc
                , field = newField
                }
            }

-- |Place the follower
gameEvents (EventKey (MouseButton LeftButton) Down _ mouse)
    state@GS
        { ls = ls@LS 
            { phase   = PlaceFollower cell
            , field   = field
            , box     = box
            , players = players
            , turns   = player:turns
            }
        } =
    if checkPlace
    then nextTurn newState
    else state --Follower wasn't placed
    where
        tc = stateMouse (is state) mouse
        threshold = 10
        checkPlace = pos `distV` tc < threshold && followerCanBePlaced field cell region
        newFollower = Follower player
        takeFollower state@PS {availFollowers = n} = state {availFollowers = n - 1}
        (pos, region) = head $ sortTileRegions cell (tiles field M.! cell) tc
        (newField, newPlayers) =
            ( placeFollower (cell, region) pos newFollower field
            , M.adjust takeFollower player players --Remove the first follower of player
            )
        newState = state
            { ls = ls
                { field   = newField
                , players = newPlayers
                }
            }

-- |Skip the follower (TODO: mouse-only)
gameEvents (EventKey (SpecialKey KeySpace) Down _ _) 
    state@GS {ls = LS {phase = PlaceFollower _}} = nextTurn state

-- |Move the mouse       
gameEvents (EventMotion mouse) state@GS {is = is}
    = state {is = is {mouse = stateMouse is mouse}}

-- |Nothing to do
gameEvents _ state = state

-- Game launch ------------------------------------------------------------------------------

-- |Viewport control configuration.
-- |Middle mouse button pans the field.
-- |Mouse wheel SHOULD scale the field, but doesn't work :(
-- |Alternatively, PageUp and PageDown scale the field.
viewStateConfig = viewStateInitWithConfig
    [ (CBumpZoomOut, [ (MouseButton WheelDown,    Nothing)
                     , (SpecialKey KeyPageDown,   Nothing) ])
    , (CBumpZoomIn,  [ (MouseButton WheelUp,      Nothing)
                     , (SpecialKey KeyPageUp,     Nothing) ])
    , (CTranslate,   [ (MouseButton MiddleButton, Nothing)
                     , (MouseButton LeftButton,   Just (Modifiers Down Up Up)) ])
    ]

-- |Finally, the function to start a Game!
playCarcaskell :: LogicState -> IO ()
playCarcaskell state =
    play (InWindow "Carcaskell v. 0.1" (floor windowWidth, floor windowHeight) (120, 120)) white 30
         GS {ls = state, is = IS {mouse = (0, 0), viewState = viewStateConfig}} render events step
